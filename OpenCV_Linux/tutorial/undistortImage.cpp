//
// Created by hazyparker on 2021/11/22.
//

#include "undistortImage.h"
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;

void De_distort(){
    // load image src
    string filePath = "../../../images/distorted.png";
    cv::Mat src = cv::imread(filePath);
    if (src.empty()){
        cout << "error! image load failed" << endl;
    }else{
        cout << "succeed, loading image..." << endl;
    }
    int rows = src.rows, cols = src.cols;

    // set distort_revise parameters
    double k1 = -0.28340811, k2 = 0.07395907, p1 = 0.00019359, p2 = 1.76187114e-05;
    // Camera Intrinsics parameters
    double fx = 458.654, fy = 457.296, cx = 367.215, cy = 248.375;

    // initialize dst
    cv::Mat dst = cv::Mat::zeros(src.size(), src.type());

    // 计算去畸变后图像的内容
    for (int v = 0; v < rows; v++) {
        for (int u = 0; u < cols; u++) {
            // 按照公式，计算点(u,v)对应到畸变图像中的坐标(u_distorted, v_distorted)
            double x = (u - cx) / fx, y = (v - cy) / fy;
            double r = sqrt(x * x + y * y);
            double x_distorted = x * (1 + k1 * r * r + k2 * r * r * r * r) + 2 * p1 * x * y + p2 * (r * r + 2 * x * x);
            double y_distorted = y * (1 + k1 * r * r + k2 * r * r * r * r) + p1 * (r * r + 2 * y * y) + 2 * p2 * x * y;
            double u_distorted = fx * x_distorted + cx;
            double v_distorted = fy * y_distorted + cy;

            cout << v << ", " << u << ", " << v_distorted << ", " << u_distorted << endl;

            // 赋值 (最近邻插值)
            if (u_distorted >= 0 && v_distorted >= 0 && u_distorted < cols && v_distorted < rows) {
                dst.at<uchar>(v, u) = src.at<uchar>((int) v_distorted, (int) u_distorted);
            } else {
                dst.at<uchar>(v, u) = 0;
                cout << v_distorted << ", " << u_distorted << endl;
            }
        }
    }

    // show image
    cv::imshow("distorted image", src);
    cv::imshow("De distort image", dst);
    cv::waitKey(0);
}