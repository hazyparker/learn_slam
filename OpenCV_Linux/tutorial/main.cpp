#include <iostream>
#include <string>
#include <opencv2/opencv.hpp>

// include headers
#include "undistortImage.h"
#include "stereoVision.h"
#include "cvFeatures.h"
#include "remapping.h"
#include "affineTrans.h"
#include "histogramEqualization.h"
#include "backProjection.h"

using namespace std;

void main_advanced(){
    // 命令行调用执行程序
    string work_name;
    cout << "type in name of function or work:" << endl;
    cin >> work_name;
    #define DEF_TEST(f) if ( work_name == #f ) return f();
    DEF_TEST(De_distort);
    DEF_TEST(showCloud);
    DEF_TEST(remapping);
    DEF_TEST(affineShow);
    DEF_TEST(histogramEqual);
    DEF_TEST(BackProjection);

    DEF_TEST(function1_0);
    DEF_TEST(function1_1);
    DEF_TEST(function1_2);
    DEF_TEST(function1_3);
    DEF_TEST(function1_4);
    DEF_TEST(function1_5);
    DEF_TEST(function1_6);
    DEF_TEST(function1_7);
    DEF_TEST(function1_8);
    DEF_TEST(function1_9);
    DEF_TEST(function1_10);
    DEF_TEST(function1_11);
    DEF_TEST(function1_12);
    DEF_TEST(function1_13);
    DEF_TEST(function1_14);
    DEF_TEST(function1_15);
    DEF_TEST(function1_16);

}

int main() {
    main_advanced();
    std::cout << "file ended" << std::endl;
    return 0;
}
