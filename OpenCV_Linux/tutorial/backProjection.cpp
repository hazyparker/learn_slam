//
// Created by hazyparker on 2021/11/24.
//

#include "backProjection.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>

using namespace std;
using namespace cv;

Mat hue;
int bins = 25;

void BackProjection(){
    cout << "Back Projection" << endl;
    cout << "refer to documentation here:" << "https://docs.opencv.org/4.5.3/da/d7f/tutorial_back_projection.html" << endl;

    // load image
    string Path = "../../../images/raw1.jpg";
    Mat src = imread(Path);
    if(src.empty()){
        cout << "Failed to load image..." << endl;
        exit(100);
    }else{
        cout << "Image loaded successfully..." << endl;
    }

    //
    Mat hsv;
    cvtColor( src, hsv, COLOR_BGR2HSV );
    hue.create(hsv.size(), hsv.depth());
    int ch[] = { 0, 0 };
    mixChannels( &hsv, 1, &hue, 1, ch, 1 );
    const char* window_image = "Source image";
    int *trackBar_pointer = nullptr;
    namedWindow( window_image, WINDOW_NORMAL );
    createTrackbar("* Hue  bins: ", window_image, &bins, 180, Hist_and_Backproj, trackBar_pointer );
    Hist_and_Backproj(0, trackBar_pointer);
    // FIXME: [ WARN:0] global /home/hazyparker/app/opencv-4.5.3/modules/highgui/src/window.cpp (704)
    //  createTrackbar UI/Trackbar(* Hue  bins: @Source image): Using 'value' pointer is unsafe and deprecated.
    //  Use NULL as value pointer. To fetch trackbar value setup callback.
    // see `createTrackbar`: https://docs.opencv.org/4.5.3/d7/dfc/group__highgui.html#gaf78d2155d30b728fc413803745b67a9b
    // this seems to be a BUG by opencv.org, add reference below
    // https://blog.csdn.net/qq_51116518/article/details/121308995
    // https://forum.opencv.org/t/creating-trackbar-in-c/5331
    imshow( window_image, src );
    // Wait until user exits the program
    waitKey();
}

void Hist_and_Backproj(int, void* ){
    int histSize = MAX( bins, 2 );
    float hue_range[] = { 0, 180 };
    const float* ranges[] = { hue_range };
    Mat hist;
    calcHist( &hue, 1, 0, Mat(), hist, 1, &histSize, ranges, true, false );
    normalize( hist, hist, 0, 255, NORM_MINMAX, -1, Mat() );
    Mat backproj;
    calcBackProject( &hue, 1, 0, hist, backproj, ranges, 1, true );
    namedWindow("BackProj", WINDOW_NORMAL);
    imshow( "BackProj", backproj );
    int w = 400, h = 400;
    int bin_w = cvRound( (double) w / histSize );
    Mat histImg = Mat::zeros( h, w, CV_8UC3 );
    for (int i = 0; i < bins; i++)
    {
        rectangle( histImg, Point( i*bin_w, h ), Point( (i+1)*bin_w, h - cvRound( hist.at<float>(i)*h/255.0 ) ),
                   Scalar( 0, 0, 255 ), FILLED );
    }
    namedWindow("Histogram", WINDOW_NORMAL);
    imshow( "Histogram", histImg );
}