
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/affineTrans.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/affineTrans.cpp.o"
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/backProjection.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/backProjection.cpp.o"
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cvFeatures.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/cvFeatures.cpp.o"
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/histogramEqualization.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/histogramEqualization.cpp.o"
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/main.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/main.cpp.o"
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/remapping.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/remapping.cpp.o"
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/stereoVision.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/stereoVision.cpp.o"
  "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/undistortImage.cpp" "/home/hazyparker/project/learn_slam/OpenCV_Linux/tutorial/cmake-build-debug/CMakeFiles/tutorial.dir/undistortImage.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_EIGEN"
  "HAVE_GLEW"
  "PANGO_DEFAULT_WIN_URI=\"wayland\""
  "PANGO_DEFAULT_WIN_URI=\"x11\""
  "_LINUX_"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../{OpenCV_INCLUDE_DIRS}"
  "/usr/local/include/opencv4"
  "/home/hazyparker/下载/Pangolin-master/components/pango_core/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_display/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_opengl/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_image/include"
  "/usr/local/include/eigen3"
  "/home/hazyparker/下载/Pangolin-master/components/pango_windowing/include"
  "/home/hazyparker/下载/Pangolin-master/build/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_vars/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_geometry/include"
  "/home/hazyparker/下载/Pangolin-master/components/tinyobj/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_glgeometry/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_packetstream/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_plot/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_scene/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_tools/include"
  "/home/hazyparker/下载/Pangolin-master/components/pango_video/include"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
