//
// Created by hazyparker on 2021/11/22.
//

#ifndef TUTORIAL_STEREOVISION_H
#define TUTORIAL_STEREOVISION_H

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include <pangolin/pangolin.h>
#include <unistd.h>

using namespace Eigen;
using namespace std;


class stereoVision {

};

void showCloud();
void showPointCloud(
        const vector<Vector4d, Eigen::aligned_allocator<Vector4d>> &pointcloud);

#endif //TUTORIAL_STEREOVISION_H
