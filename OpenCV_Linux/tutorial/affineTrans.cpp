//
// Created by parker on 2021/11/23.
//

#include "affineTrans.h"
#include <opencv2/opencv.hpp>
#include <iostream>

using namespace std;
using namespace cv;

void affineShow(){
    // load image src
    string Path = "../../../images/cloud.png";
    Mat src = imread(Path);
    if(src.empty()){
        cout << "Failed to load image..." << endl;
        exit(100);
    }else{
        cout << "Image loaded successfully..." << endl;
    }

    // set Point2 in float
    Point2f srcTri[3];
    srcTri[0] = Point2f( 0.f, 0.f );
    srcTri[1] = Point2f( (float)src.cols - 1.f, 0.f );
    srcTri[2] = Point2f( 0.f, (float)src.rows - 1.f );
    Point2f dstTri[3];
    dstTri[0] = Point2f( 0.f, (float)src.rows*0.33f );
    dstTri[1] = Point2f( (float)src.cols*0.85f, (float)src.rows*0.25f );
    dstTri[2] = Point2f( (float)src.cols*0.15f, (float)src.rows*0.7f );

    // wrap
    Mat warp_mat = getAffineTransform( srcTri, dstTri );
    Mat warp_dst = Mat::zeros( src.rows, src.cols, src.type() );
    warpAffine( src, warp_dst, warp_mat, warp_dst.size() );

    // wrap + rotate
    Point center = Point( warp_dst.cols/2, warp_dst.rows/2 );
    double angle = -50.0;
    double scale = 0.6;
    Mat rot_mat = getRotationMatrix2D( center, angle, scale );
    Mat warp_rotate_dst;
    warpAffine( warp_dst, warp_rotate_dst, rot_mat, warp_dst.size() ); // rotation matrix provided

    // show Transferred images
    imshow( "Source image", src );
    imshow( "Warp", warp_dst );
    imshow( "Warp + Rotate", warp_rotate_dst );
    waitKey();
}

