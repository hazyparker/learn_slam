//
// Created by hazyparker on 2021/11/23.
//

#include "histogramEqualization.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>

using namespace std;
using namespace cv;

void histogramEqual(){
    cout << "Histogram Equalization" << endl;

    // load image src
    string Path = "../../../images/raw1.jpg";
    Mat src = imread(Path);
    if(src.empty()){
        cout << "Failed to load image..." << endl;
        exit(100);
    }else{
        cout << "Image loaded successfully..." << endl;
    }

    // Histogram Equalization
    cvtColor( src, src, COLOR_BGR2GRAY );
    Mat dst;
    equalizeHist( src, dst );

    // show image
    namedWindow("Source image", WINDOW_NORMAL);
    namedWindow("Equalized image", WINDOW_NORMAL);
    imshow( "Source image", src );
    imshow( "Equalized image", dst );
    waitKey();
}
