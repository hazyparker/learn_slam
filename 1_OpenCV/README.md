#  OpenCV学习

1. 通过跟随[视频](https://www.bilibili.com/video/BV1uW411d7Wf?p=2)来进行OpenCv的学习，操作平台为windows下的VS2017，并且参考[文档](http://www.opencv.org.cn/opencvdoc/2.3.2/html/doc/tutorials/imgproc/table_of_content_imgproc/table_of_content_imgproc.html#table-of-content-imgproc)进行学习。
2. 计划每天看五个视频并查阅来进行OpenCV的学习与练习。

## 目标

1. 通过基础入门的学习，进一步加强对c++的理解
2. 学会使用opencv的基本操作与一些简单的原理



## 学习内容

1. Mat操作

``` cpp
Mat src;//定义一个Mat类型的变量
Mat m1;
m1.create(src.size(), src.type());//令m1与src类型大小一致
//**********把src的内容复制给dst*****************/
Mat dst = src.clone();
//src.copyTo(dst);同上
```

2.  图片的加载与显示 

``` cpp
Mat src = imread("F:/opencv/car.jpg");//加载一张图片
Mat dst;
//常用 src.empty(),或者src.data() 来判断数据是否图片成功
if (src.empty()) {
		cout << "coule not load image";
}
cvtColor(src, dst, CV_RGB2GRAY);//转换函数，吧彩色图像转换成灰度图片
imshow("Output", dst);//显示一张Mat图片
imwrite("F:/opencv/carBlack.png", dst);//图片的存储
```
图片的像素操作，设定gray_src为灰度图片，dst为彩色图片
``` cpp
//获取灰度图片的行与列
int height = gray_src.rows;
int wight = gray_src.cols;
//读取灰色图片像素值
int gray = gray_src.at<uchar>(row, col);

//依次读取彩色图片RGB值
int height = dst.rows;
int wight = dst.cols;
int b = dst.at<Vec3b>(row, col)[0];
int g = dst.at<Vec3b>(row, col)[1];
int r = dst.at<Vec3b>(row, col)[2];
```

3. [光流法](https://www.cnblogs.com/penuel/p/13425277.html)